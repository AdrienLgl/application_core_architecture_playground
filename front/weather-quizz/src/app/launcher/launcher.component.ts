import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { NavService } from '../_services/nav.service';

@Component({
  selector: 'app-launcher',
  templateUrl: './launcher.component.html',
  styleUrls: ['./launcher.component.scss']
})
export class LauncherComponent implements OnInit {
  panelOpenState = false;
  questions = [10, 20, 30];
  form: FormGroup;

  constructor(public navService: NavService) {
    this.form = new FormGroup({ questionsNumbers: new FormControl(10) });
    this.navService.hide();
  }

  ngOnInit(): void { }

}
