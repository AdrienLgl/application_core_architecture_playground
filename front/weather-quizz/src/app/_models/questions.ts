export interface Response {
    response: string;
    good: boolean;
}

export interface FullResponses {
    A: Response;
    B: Response;
    C: Response;
    D: Response;
}

export interface Player {
    player: string;
    zipCode: string;
    city: string;
}

export interface Score {
    player: string;
    score: number;
}
