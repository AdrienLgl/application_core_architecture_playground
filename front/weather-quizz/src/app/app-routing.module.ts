import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MultiConfigurationComponent } from './configuration/multi-configuration/multi-configuration.component';
import { SoloConfigurationComponent } from './configuration/solo-configuration/solo-configuration.component';
import { HomeComponent } from './home/home.component';
import { LauncherComponent } from './launcher/launcher.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './login/register/register.component';
import { QuestionsComponent } from './questions/questions.component';
import { AuthGuard } from './_helpers/auth.guard';
import { PodiumComponent } from './podium/podium.component';
import { NavGuard } from './_helpers/nav.guard';

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'launcher', component: LauncherComponent, canDeactivate: [NavGuard], canActivate: [AuthGuard] },
  { path: 'solo', component: SoloConfigurationComponent, canDeactivate: [NavGuard], canActivate: [AuthGuard] },
  { path: 'multi', component: MultiConfigurationComponent, canDeactivate: [NavGuard], canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent, canDeactivate: [NavGuard] },
  { path: 'register', component: RegisterComponent, canDeactivate: [NavGuard] },
  { path: 'questions', component: QuestionsComponent, canDeactivate: [NavGuard] },
  {
    path: 'podium', component: PodiumComponent, canDeactivate: [NavGuard]
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
