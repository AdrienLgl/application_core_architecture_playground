import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiConfigurationComponent } from './multi-configuration.component';

describe('MultiConfigurationComponent', () => {
  let component: MultiConfigurationComponent;
  let fixture: ComponentFixture<MultiConfigurationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MultiConfigurationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
