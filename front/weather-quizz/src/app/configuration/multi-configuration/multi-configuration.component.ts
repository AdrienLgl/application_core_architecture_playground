import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NavService } from 'src/app/_services/nav.service';
import { QuizzService } from 'src/app/_services/quizz.service';

@Component({
  selector: 'app-multi-configuration',
  templateUrl: './multi-configuration.component.html',
  styleUrls: ['./multi-configuration.component.scss']
})
export class MultiConfigurationComponent implements OnInit {
  form: FormGroup;
  cities = [
    {
      city: 'Rennes',
      zipCode: '35000'
    },
    {
      city: 'Paris',
      zipCode: ''
    }
  ];
  constructor(nav: NavService, private fb: FormBuilder, private router: Router, private quizzService: QuizzService) {
    nav.hide();
    this.form = this.fb.group({
      title: new FormControl('Multi'),
      players: this.fb.array([
        this.fb.group({ name: new FormControl('Player1'), city: new FormControl(null), zipCode: new FormControl(null)}),
        this.fb.group({ name: new FormControl('Player2'), city: new FormControl(null), zipCode: new FormControl(null)})
      ])
    });
  }

  ngOnInit(): void { }


  addCity(value: string): void {
    this.cities.push({city: value, zipCode: null});
  }

  getZipCode(city: string): string {
    return this.cities.filter(element => element.city === city)[0].zipCode;
  }

  get players(): FormArray {
    return this.form.get('players') as FormArray;
  }

  newPlayer(): FormGroup {
    return this.fb.group({
      name: `Player${this.players.length + 1}`,
      city: null,
      zipCode: null
    });
  }

  addPlayer(): void {
    this.players.push(this.newPlayer());
  }

  removePlayer(i: number): void {
    this.players.removeAt(i);
  }

  launchGame(): void {
    console.log(this.players.value);
    this.quizzService.addNewPlayer(this.players.value);
    this.router.navigate(['questions']);
  }
}
