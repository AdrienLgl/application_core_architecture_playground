import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NavService } from 'src/app/_services/nav.service';
import { QuizzService } from 'src/app/_services/quizz.service';

@Component({
  selector: 'app-solo-configuration',
  templateUrl: './solo-configuration.component.html',
  styleUrls: ['./solo-configuration.component.scss']
})
export class SoloConfigurationComponent implements OnInit {
  form: FormGroup;
  cities = [
    {
      city: 'Rennes',
      zipCode: '35000'
    },
    {
      city: 'Paris',
      zipCode: ''
    }
  ];

  constructor(nav: NavService, private quizzService: QuizzService, private router: Router) {
    nav.hide();
    this.form = new FormGroup({ name: new FormControl(null), city: new FormControl(null) });
  }

  ngOnInit(): void { }

  getZipCode(city: string): string {
    return this.cities.filter(element => element.city === city)[0].zipCode;
  }

  startGame(): void {
    let player = this.form.get('name').value;
    if (player === null) { player = 'Player'; }
    this.quizzService.addNewPlayer([
      {
        city: this.form.get('city').value,
        zipCode: this.getZipCode(this.form.get('city').value),
        name: player
      }
    ]);
    this.router.navigate(['questions']);
  }

}
