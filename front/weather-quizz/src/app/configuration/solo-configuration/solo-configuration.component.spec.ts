import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SoloConfigurationComponent } from './solo-configuration.component';

describe('SoloConfigurationComponent', () => {
  let component: SoloConfigurationComponent;
  let fixture: ComponentFixture<SoloConfigurationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SoloConfigurationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SoloConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
