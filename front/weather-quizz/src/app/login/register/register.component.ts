import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NavService } from 'src/app/_services/nav.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['../login.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;

  constructor(nav: NavService, private router: Router) {
    nav.hide();
    this.registerForm = new FormGroup({
      email: new FormControl(null),
      pseudo: new FormControl(null),
      password: new FormControl(null, [Validators.required]),
      passwordConfirmation: new FormControl(null)
    }, { validators: this.checkPasswords });
  }


  ngOnInit(): void {
  }

  register(): void {
    const user = {
      mail: this.registerForm.get('email').value,
      login: this.registerForm.get('pseudo').value,
      password: this.registerForm.get('password').value
    };
    if (user.mail !== null && user.login !== null && user.password !== null) {
      localStorage.setItem('user', JSON.stringify({login: user.login}));
      this.router.navigate(['/home']);
    }
  }

  checkPasswords(group: FormGroup): any {
    const password = group.get('password').value;
    const confirmPassword = group.get('passwordConfirmation').value;
    return password === confirmPassword ? null : { notSame: true };
  }

}
