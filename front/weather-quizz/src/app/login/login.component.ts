import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NavService } from '../_services/nav.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  loginForm: FormGroup;

  constructor(nav: NavService, private router: Router) {
    nav.hide();
    this.loginForm = new FormGroup({
      login: new FormControl(null),
      password: new FormControl(null)
    });
  }

  ngOnInit(): void {
  }

  login(): void {
    if (localStorage.getItem('user') !== null && JSON.parse(localStorage.getItem('user')).login === this.loginForm.get('login').value) {
      this.setSession(JSON.parse(localStorage.getItem('user')));
    }
  }

  setSession(user): void {
    localStorage.setItem('session', JSON.stringify(user.login));
    this.router.navigate(['/home']);
  }

}
