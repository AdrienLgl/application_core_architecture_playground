import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Player } from '../_models/questions';

const httpOptions = {
  headers: new HttpHeaders({
    Authorization: '563492ad6f917000010000014060d806c66c47b88b9b4d7f8c487692'
  })
};

@Injectable({
  providedIn: 'root'
})

export class QuizzService {

  constructor(private http: HttpClient) { }

  getWeatherImages(): Observable<any> {
    const url = 'https://api.pexels.com/v1/search?query=weather';
    return this.http.get<any>(url, httpOptions).pipe(catchError(this.handelError));
  }

  handelError(error): any {
    return throwError(error.message || 'Server Error');
  }

  removeAllPlayers(): void {
    localStorage.removeItem('players');
  }

  getAllPlayers(): any[] {
    return JSON.parse(localStorage.getItem('players'));
  }

  addNewPlayer(players: any[]): void {
    const finalPlayers = [];
    if (players.length > 0) {
      players.forEach((player) => {
        finalPlayers.push({
          name: player.name,
          zipCode: player.zipCode,
          score: 0
        });
      });
      localStorage.setItem('players', JSON.stringify(finalPlayers));
    }
  }
}
