import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NavService {

  visible: boolean;

  constructor() { this.visible = true; }

  hide(): void { this.visible = false; }
  show(): void { this.visible = true; }
}
