import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private router: Router) { }


  isLoggedIn(): boolean {
    if (localStorage.getItem('session') !== null) {
      return true;
    }
    this.router.navigate(['/login']);
    return false;
  }

  logout(): void {
    localStorage.removeItem('session');
    this.router.navigate(['/login']);
  }
}
