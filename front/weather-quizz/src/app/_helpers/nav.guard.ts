import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { LauncherComponent } from '../launcher/launcher.component';
import { NavService } from '../_services/nav.service';

@Injectable({
  providedIn: 'root'
})
export class NavGuard implements CanDeactivate<unknown> {

  constructor(private nav: NavService) { }

  canDeactivate(target: LauncherComponent): boolean {
    this.nav.show();
    return true;
  }

}
