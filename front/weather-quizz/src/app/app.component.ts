import { Component } from '@angular/core';
import { NavService } from './_services/nav.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'weather-quizz';

  constructor(public navService: NavService) { }

}
