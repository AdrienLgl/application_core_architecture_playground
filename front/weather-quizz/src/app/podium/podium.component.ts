import { Component, OnInit } from '@angular/core';
import { NavService } from '../_services/nav.service';
import { QuizzService } from '../_services/quizz.service';

@Component({
  selector: 'app-podium',
  templateUrl: './podium.component.html',
  styleUrls: ['./podium.component.scss']
})
export class PodiumComponent implements OnInit {

  playersResult = [];

  constructor(public navService: NavService, private quizzService: QuizzService) {
    this.navService.hide();
    this.playersResult = this.quizzService.getAllPlayers();
    this.playersResult = this.playersResult.sort((a, b) => a.score - b.score);
  }

  ngOnInit(): void {
  }

}
