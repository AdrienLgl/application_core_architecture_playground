import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { FullResponses, Score } from '../_models/questions';
import { NavService } from '../_services/nav.service';
import { QuizzService } from '../_services/quizz.service';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss'],
  encapsulation: ViewEncapsulation.None,
})



export class QuestionsComponent implements OnInit {

  weatherImageUrl: string;
  question: string;
  responses: FullResponses;
  response: boolean;
  timeLeft = 30;
  interval;
  score: Score[];
  currentPlayer = 0;
  totalQuestions = 0;
  end = false;

  constructor(nav: NavService, private quizzService: QuizzService, private router: Router) {
    nav.hide();
    this.score = this.setScore();
    this.setNewQuestion();
  }

  ngOnInit(): void { }

  setScore(): Score[] { // Get players informations
    const players = JSON.parse(localStorage.getItem('players'));
    return players;
  }

  newTurn(): void { // New turn
    this.totalQuestions += 1;
    if (this.totalQuestions === 10) { this.endGame(); }
    if (this.response) { this.score[this.currentPlayer].score += 10; }
    if (this.currentPlayer < this.score.length - 1) {
      this.currentPlayer = this.currentPlayer + 1;
    } else {
      this.currentPlayer = 0;
    }
    this.setNewQuestion();
  }

  setImage(): void {
    this.quizzService.getWeatherImages().subscribe(data => {
      const index = Math.floor(Math.random() * 15);
      this.weatherImageUrl = data.photos[index].src.landscape;
    });
  }

  startTimer(): void {
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        this.response === undefined ? this.response = false : this.response = this.response;
        this.stopTimer();
      }
    }, 1000);
  }

  endGame(): void { // End of the game
    this.end = true;
    localStorage.setItem('players', JSON.stringify(this.score));
    setTimeout(() => {
      this.router.navigate(['podium']);
    }, 3000);
  }

  pauseTimer(): void {
    clearInterval(this.interval);
  }

  stopTimer(): void {
    this.timeLeft = 30;
    clearInterval(this.interval);
  }

  resetTimer(): void {
    this.timeLeft = 30;
    this.startTimer();
  }

  selectResponse(response: boolean): void { // Select a response
    if (this.response === undefined) {
      this.response = response;
      this.stopTimer();
    }
  }

  setNewQuestion(): void { // Create new question
    const question = new Question('35550', this.quizzService);
    this.response = question.finalResponse;
    this.responses = question.responses;
    this.question = question.question;
    this.setImage();
    this.startTimer();
  }

}


export class Question {

  question: string;
  responses: FullResponses;
  finalResponse: boolean = undefined;

  constructor(private zipCode: string, quizzService: QuizzService) {
    this.question = 'Nouvelle question ?';
    this.responses = this.getResponses();
  }

  getResponses(): FullResponses { // Should return question's API response
    return {
      A: {
        response: 'Response A',
        good: false
      },
      B: {
        response: 'Response B',
        good: true
      },
      C: {
        response: 'Response C',
        good: false
      },
      D: {
        response: 'Response D',
        good: false
      }
    };
  }

}
