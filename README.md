# Beat my neighbor !

## Front

Le front de ce projet a été réalisé avec le framework Angular qui nous paraissait adapté pour intégrer facilement des données d'API.

Pour lancer le front, il vous faudra :  
- Installer Angular (https://angular.io/guide/setup-local)
- Installer nodeJS (https://nodejs.org/en/download/)
- Lancer le projet à partir de la racine (/front/weather-quizz) avec la commande ng serve

**Version 1.0.0**

- Interface de jeu
- Choix des joueurs (solo ou multi)
- Choix des pseudos
- Affichage des questions pour chaque joueur
- Fin de la partie au bout de 10 questions
- Questions et réponses fixes
- ScoreBoard
- Login (but not activated)

## Back

Api en laravel 8  

Installer le projet :
- composer install 
- création du .env sur modèle du .env.example
- lancer le projet : php artisan serve  

Pour voir le fonctionnement des tables du projet, voir mcd dans dossier Document/mcd.  

Description : une application web en localhost permettant de faire un quizz météo en mode solo ou multi joueur(s).  

L'api permet, pour un jeu en mode solo,  de se connecter ou de s'inscrire grâce au module breeze.  

Une fois connecter l'utilisateur peut créer une partie ou consulter son scoreboard qui
récupère uniquement ces parties en mode solo par ordre chronologique.  

Pour créer une partie, le joueur doir choisir une ville.  
Une fois la partie créée, l'api récupère les 10 premières questions qui on été le moins utilisé
afin de les renvoyer une par une.  

La partie réponse n'est pas gérer actuellement, cependant l'idée
était de d'avoir 3 réponses fausse récupérer depuis la table réponse et que la bonne réponse soit
généré à partir des valeurs des tables qui gère la partie weather.  

Le mode multi fonctionne de la même facon, sauf que chaque joueur choisit une ville et que le nombre
de questions est à choisir dans une liste.


## Description du projet :

BMN! est un jeu, permettant à des personnes de réaliser des paris météo.
De un à quatre joueurs, cette application se joue en local multi-player. C'est à dire, tous ensemble devant le même clavier.
Un peu semblable à un quizz, les joueurs devront sélectionner la réponse qui leur semble être la bonne parmi une liste de réponses oossibles.

Techniquement, l'application est structurée en architecture 2-tiers.
D'un côté un front, gamifié pour les joueurs et connecté à une API.
L'autre, le back, consistant en une API permettant de générer les questions et stockant les résultats.

L'ensemble des données utilisées par l'application a été généré par un script python présent au sein du dépôt: [https://gitlab.com/docusland/python-weather-webservice](https://gitlab.com/docusland/python-weather-webservice)

Ce code a initiallement été réalisé par [EnzoKilm](https://github.com/EnzoKilm)

## Quizz

Si l'utilisateur souhaite jouer seul, il existe un mode quizz. Lui posant des questions génériques basées sur l'ensemble des données issues de la base de données.

Une suite de 10 questions lui seront posées.
L'utilisateur saisira sa réponse en cliquant sur des choix possibles.
En fin de partie, il pourra voir son ranking face à d'autres joueurs ayant joué précédemment.

Voici un exemple de questions posées :

- Quel a été le jour le plus chaud dans la ville de Toulouse ?

Les questions sont générées depuis le Back end.

## Challenge

En mode challenge, il est attendu à ce que tous les joueurs soient face au même ordinateur. Ils joueront tout simplement à tour de rôle.

En début de partie, ils devront préciser leurs pseudos ainsi que leurs communes et le nombre de questions que durera ce challenge.

Les joueurs devront choisir des pseudo et des communes distinctes.
Les communes proposées devront correspondre à des communes présentes dans la base de données.

A chaque tour de jeu, une question leur sera posée et chaque joueur devra voter pour un symbole de météo.

Exemple de questions :

- Le 6 mai 2021 à 19h, quelle était la météo chez vous ?
- Au mois de mai, il y a eu une majorité de 'Cloudy'
- Dans vos communes respectives, en mai 2021, il a fait 'Cloudy' le plus chez qui ?
- Chez qui est-il le plus pertinent d'organiser les barbecues les dimanches midis?

Les questions proposées doivent être générées à partir des données qui sont en base.

Chaque question apporte des points aux joueurs.

Une fois que le nombre de questions est passé, les joueurs voient leurs scores et celui qui gagne.