<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cities_weathers extends Model
{
    use HasFactory;

    protected $fillable = [
      'cities_weathers_current'
    ];

    public $timestamps = false;
}
