<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MultiGames extends Model
{
    use HasFactory;

    protected $fillable = [
      'multi_games_nb_questions'
    ];

    public $timestamps = false;

    public function players(){
        return $this->belongsToMany(Players::class);
    }

    public function questions(){
        return $this->belongsToMany(Questions::class);
    }
}
