<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlayersMultiGames extends Model
{
    use HasFactory;

    protected $fillable = [
      'players_multi_games_point'
    ];

    public $timestamps = false;
}
