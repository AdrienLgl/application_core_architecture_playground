<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
    use HasFactory;

    protected $fillable = [
        'questions_libelle',
        'questions_nb_use'
    ];

    public $timestamps = false;

    public function answers(){
        return $this->belongsToMany(Answers::class);
    }

    public function solo_games(){
        return $this->belongsToMany(SoloGames::class);
    }

    public function multi_games(){
        return $this->belongsToMany(MultiGames::class);
    }
}
