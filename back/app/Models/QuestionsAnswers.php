<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuestionsAnswers extends Model
{
    use HasFactory;

    protected $fillable = [
      'questions_answers_good_answers'
    ];

    public $timestamps = false;
}
