<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Weathers extends Model
{
    use HasFactory;

    protected $fillable =[
      'weathers_date',
      'weathers_min',
      'weathers_max'
    ];

//    public bool $timestamps = false;

    public function cities(){
        return $this->belongsToMany(Cities::class);
    }

}
