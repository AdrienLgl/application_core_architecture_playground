<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Answers extends Model
{
    use HasFactory;

    protected $fillable = [
      'answers_libelle'
    ];

    public $timestamps = false;

    public function questions(){
        return $this->belongsToMany(Questions::class);
    }
}
