<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SoloGames extends Model
{
    use HasFactory;

    protected $fillable =[
      'solo_games_point'
    ];

    public $timestamps = false;

    public function questions(){
        return $this->belongsToMany(Questions::class);
    }
}
