<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cities extends Model
{
    use HasFactory;

    protected $fillable = [
      'cities_name',
      'cities_zipcode'
    ];

    public $timestamps = false;

    public function weathers(){
        return $this->belongsToMany(Weathers::class);
    }

}
