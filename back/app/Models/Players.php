<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Players extends Model
{
    use HasFactory;

    protected $fillable = [
      'players_name',
      'players_city',
      'players_has_an_account'
    ];

    public $timestamps = false;

    public function multi_games(){
        return $this->belongsToMany(MultiGames::class);
    }
}
