<?php

namespace App\Http\Controllers;

use App\Http\Resources\MultiGamesQuestionsResource;
use App\Models\MultiGamesQuestions;
use App\Models\SoloGamesQuestions;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class MultiGamesQuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return MultiGamesQuestionsResource
     */
    public function index($Code_multi_games, $numeroQuestion)
    {
        $question = DB::table('multi_games_questions')->where('Code_multi_games', '=',
            $Code_multi_games)->get();
        for ($i = 1; $i < 11; $i++){
            if ($i == $numeroQuestion){
                return new MultiGamesQuestionsResource($question[$i]);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lastMultiGame = DB::table('multi_games')->latest()->get();
        $questions = DB::table('questions')->orderByDesc('questions_nb_use')->limit(10)->get();
        for ($i = 0; $i<10; $i++){
            SoloGamesQuestions::create([
                'Code_questions'=>$questions[$i]['Code_questions'],
                'Code_solo_games' => $lastMultiGame['Code_multi_games']
            ]);
            (new QuestionsController)->update($questions[$i]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MultiGamesQuestions  $multiGamesQuestions
     * @return \Illuminate\Http\Response
     */
    public function show(MultiGamesQuestions $multiGamesQuestions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MultiGamesQuestions  $multiGamesQuestions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MultiGamesQuestions $multiGamesQuestions)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MultiGamesQuestions  $multiGamesQuestions
     * @return \Illuminate\Http\Response
     */
    public function destroy(MultiGamesQuestions $multiGamesQuestions)
    {
        //
    }
}
