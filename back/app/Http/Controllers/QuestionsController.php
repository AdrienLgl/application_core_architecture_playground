<?php

namespace App\Http\Controllers;

use App\Http\Resources\QuestionsResource;
use App\Models\Questions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Question\Question;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return QuestionsResource
     */
    public function store(Request $request)
    {
        $Question = new Questions();
        $Question->questions_libelle = $request->questions_libelle;
        $Question->questions_nb_use = 0;
        if ($Question ->save()){
            return new QuestionsResource($Question);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Questions  $questions
     * @return \Illuminate\Http\Response
     */
    public function show(Questions $questions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Questions  $questions
     * @return \Illuminate\Http\Response
     */
    public function update(Question $question)
    {
        DB::table('questions')->where('Code_questions', '=', $question['Code_questions'])->
        update(['questions_nb_use'=>$question['questions_nb_use'] + 1 ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Questions  $questions
     * @return \Illuminate\Http\Response
     */
    public function destroy(Questions $questions)
    {
        //
    }
}
