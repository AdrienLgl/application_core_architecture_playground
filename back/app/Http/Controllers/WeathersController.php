<?php

namespace App\Http\Controllers;

use App\Http\Resources\WeathersResource;
use App\Models\Weathers;
use Illuminate\Http\Request;

class WeathersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return WeathersResource
     */
    public function store(Request $request)
    {
        $Weather = new Weathers();
        $Weather->weathers_date = now();
        $Weather->weathers_min = $request->weathers_min;
        $Weather->weathers_max = $request->weathers_max;
        if ($Weather->save()){
            return new WeathersResource($Weather);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Weathers  $weathers
     * @return \Illuminate\Http\Response
     */
    public function show(Weathers $weathers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Weathers  $weathers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Weathers $weathers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Weathers  $weathers
     * @return \Illuminate\Http\Response
     */
    public function destroy(Weathers $weathers)
    {
        //
    }
}
