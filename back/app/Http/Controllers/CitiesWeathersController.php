<?php

namespace App\Http\Controllers;

use App\Models\cities_weathers;
use Illuminate\Http\Request;

class CitiesWeathersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\cities_weathers  $cities_weathers
     * @return \Illuminate\Http\Response
     */
    public function show(cities_weathers $cities_weathers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\cities_weathers  $cities_weathers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, cities_weathers $cities_weathers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\cities_weathers  $cities_weathers
     * @return \Illuminate\Http\Response
     */
    public function destroy(cities_weathers $cities_weathers)
    {
        //
    }
}
