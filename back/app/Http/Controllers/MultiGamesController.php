<?php

namespace App\Http\Controllers;

use App\Http\Resources\MultiGameResource;
use App\Models\MultiGames;
use App\Models\PlayersMultiGames;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MultiGamesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return MultiGameResource
     */
    public function store(Request $request)
    {
        $MultiGame = new MultiGames();
        $MultiGame->multi_games_nb_questions = $request->multi_games_nb_questions;
        $Players = DB::table('players')->orderByDesc('Code_players')->limit(2)->get();
        if ($MultiGame->save()){
            for ($i = 0; $i < 2; $i++) {
                PlayersMultiGames::create([
                    'Code_players'=>$Players[$i]['Code_players'],
                    'Code_multi_games'=> $MultiGame['Code_multi_game'],
                    'players_multi_games_point' => 0
                ]);
            }
            return new MultiGameResource($MultiGame);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MultiGames  $multiGames
     * @return \Illuminate\Http\Response
     */
    public function show(MultiGames $multiGames)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MultiGames  $multiGames
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MultiGames $multiGames)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MultiGames  $multiGames
     * @return \Illuminate\Http\Response
     */
    public function destroy(MultiGames $multiGames)
    {
        //
    }
}
