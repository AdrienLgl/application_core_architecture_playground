<?php

namespace App\Http\Controllers;

use App\Models\PlayersMultiGames;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PlayersMultiGamesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PlayersMultiGames  $playersMultiGames
     * @return \Illuminate\Http\Response
     */
    public function show(PlayersMultiGames $playersMultiGames)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PlayersMultiGames  $playersMultiGames
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PlayersMultiGames $playersMultiGames)
    {
        DB::table('players_multi_games')->where('Code_multi_games','=', $request->Code_multi_games)
            ->where('Code_players', '=', $request->Code_players )
            ->update(['players_multi_games_point' => $request->players_multi_games_point]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PlayersMultiGames  $playersMultiGames
     * @return \Illuminate\Http\Response
     */
    public function destroy(PlayersMultiGames $playersMultiGames)
    {
        //
    }
}
