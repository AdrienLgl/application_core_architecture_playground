<?php

namespace App\Http\Controllers;

use App\Http\Resources\QuestionsResource;
use App\Http\Resources\SoloGamesQuestionsResource;
use App\Models\Questions;
use App\Models\SoloGamesQuestions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SoloGamesQuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return SoloGamesQuestionsResource
     */
    public function index($Code_game_solo, $numeroQuestion)
    {
        $question = DB::table('solo_games_questions')->where('Code_solo_games', '=', $Code_game_solo)->get();
        for ($i = 1; $i < 11; $i++){
            if ($i == $numeroQuestion){
                return new SoloGamesQuestionsResource($question[$i]);
            }
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lastSoloGame = DB::table('solo_games')->latest()->get();
        $questions = DB::table('questions')->orderByDesc('questions_nb_use')->limit(10)->get();
        for ($i = 0; $i<10; $i++){
            SoloGamesQuestions::create([
                'Code_questions'=>$questions[$i]['Code_questions'],
                'Code_solo_games' => $lastSoloGame['Code_solo_games']
            ]);
            (new QuestionsController)->update($questions[$i]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SoloGamesQuestions  $soloGamesQuestions
     * @return \Illuminate\Http\Response
     */
    public function show(SoloGamesQuestions $soloGamesQuestions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SoloGamesQuestions  $soloGamesQuestions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SoloGamesQuestions $soloGamesQuestions)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SoloGamesQuestions  $soloGamesQuestions
     * @return \Illuminate\Http\Response
     */
    public function destroy(SoloGamesQuestions $soloGamesQuestions)
    {
        //
    }
}
