<?php

namespace App\Http\Controllers;

use App\Http\Resources\PlayersResource;
use App\Models\Players;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PlayersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return PlayersResource
     */
    public function store(Request $request)
    {
        if ($request->players_name != null){
            $Players = new Players();
            $Players->$this->players_name = $request->players_name;
            $Players->$this->players_city = $request->players_city;
            $Players->$this->players_has_an_account = $request->players_has_an_account;
            if ($Players->save()){
                return new PlayersResource($Players);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Players  $players
     * @return \Illuminate\Http\Response
     */
    public function show(Players $players)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Players  $players
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Players $players)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Players  $players
     * @return \Illuminate\Http\Response
     */
    public function destroy(Players $players)
    {
        //
    }
}
