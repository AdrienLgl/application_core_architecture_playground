<?php

namespace App\Http\Controllers;

use App\Http\Resources\SoloGamesResources;
use App\Models\SoloGames;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SoloGamesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return SoloGamesResources
     */
    public function store(Request $request)
    {
        $soloGame = new SoloGames();
        $soloGame->solo_games_city = $request->solo_games_city;
        $soloGame->solo_games_point = 0;
        $soloGame->Code_users = $request->Code_users;
        if($soloGame->save())
        {
            return new SoloGamesResources($soloGame);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SoloGames  $soloGames
     * @return \Illuminate\Http\Response
     */
    public function show(SoloGames $soloGames)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SoloGames  $soloGames
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SoloGames $soloGames)
    {
        DB::table('solo_game')->where('Code_solo_games', '=', $soloGames['Code_solo_games'])
            ->update(['solo_games_point'=> $request->solo_games_point]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SoloGames  $soloGames
     * @return \Illuminate\Http\Response
     */
    public function destroy(SoloGames $soloGames)
    {
        //
    }

    public function getScoreBoard()
    {
        $scoreboard = User::displayScoreBoardSoloGame();
        return $scoreboard->toJson();
    }
}
