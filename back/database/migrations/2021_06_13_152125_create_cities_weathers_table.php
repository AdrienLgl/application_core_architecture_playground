<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitiesWeathersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities_weathers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('Code_cities')->constrained();
            $table->foreignId('Code_weathers')->constrained();
            $table->float('cities_weathers_current');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities_weathers');
    }
}
