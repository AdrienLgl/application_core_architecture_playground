<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlayersMultiGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players_multi_games', function (Blueprint $table) {
            $table->id();
            $table->foreignId('Code_players')->constrained();
            $table->foreignId('Code_multi_games')->constrained();
            $table->integer('players_multi_games_point');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players_multi_games');
    }
}
