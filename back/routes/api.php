<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CitiesController;
use App\Http\Controllers\AnswersController;
use App\Http\Controllers\CitiesWeathersController;
use App\Http\Controllers\MultiGamesController;
use App\Http\Controllers\MultiGamesQuestionsController;
use App\Http\Controllers\PlayersController;
use App\Http\Controllers\PlayersMultiGamesController;
use App\Http\Controllers\QuestionsAnswersController;
use App\Http\Controllers\QuestionsController;
use App\Http\Controllers\SoloGamesController;
use App\Http\Controllers\SoloGamesQuestionsController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WeathersController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\EmailVerificationNotificationController;
use App\Http\Controllers\Auth\EmailVerificationPromptController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\Auth\VerifyEmailController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// route pour connexion/inscription

Route::get('/register', [RegisteredUserController::class, 'create'])
    ->middleware('guest')
    ->name('register');

Route::post('/register', [UserController::class, 'store'])
    ->middleware('guest');

Route::get('/login', [AuthenticatedSessionController::class, 'create'])
    ->middleware('guest')
    ->name('login');

Route::post('/login', [AuthenticatedSessionController::class, 'store'])
    ->middleware('guest');

Route::get('/forgot-password', [PasswordResetLinkController::class, 'create'])
    ->middleware('guest')
    ->name('password.request');

Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])
    ->middleware('guest')
    ->name('password.email');

Route::get('/reset-password/{token}', [NewPasswordController::class, 'create'])
    ->middleware('guest')
    ->name('password.reset');

Route::post('/reset-password', [NewPasswordController::class, 'store'])
    ->middleware('guest')
    ->name('password.update');

Route::get('/verify-email', [EmailVerificationPromptController::class, '__invoke'])
    ->middleware('auth')
    ->name('verification.notice');

Route::get('/verify-email/{id}/{hash}', [VerifyEmailController::class, '__invoke'])
    ->middleware(['auth', 'signed', 'throttle:6,1'])
    ->name('verification.verify');

Route::post('/email/verification-notification', [EmailVerificationNotificationController::class, 'store'])
    ->middleware(['auth', 'throttle:6,1'])
    ->name('verification.send');

Route::get('/confirm-password', [ConfirmablePasswordController::class, 'show'])
    ->middleware('auth')
    ->name('password.confirm');

Route::post('/confirm-password', [ConfirmablePasswordController::class, 'store'])
    ->middleware('auth');

Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
    ->middleware('auth')
    ->name('logout');


// recuperer les villes

Route::get('cities', [CitiesController::class, 'index']);

// Route weather

Route::post('weather', [WeathersController::class, 'store']);

// route lie a une partie solo ou multi

Route::post('modesolo', [SoloGamesController::class, 'store']);

Route::put('modesolo/end', [SoloGamesController::class, 'update']);

Route::post('players/{n}', [PlayersController::class, 'store'])->where('n', '[1,4]');

Route::post('modemulti', [MultiGamesController::class, 'store']);

Route::put('modemulti/end', [PlayersMultiGamesController::class, 'update']);

Route::post('answers', [AnswersController::class, 'store']);

Route::post('question', [AnswersController::class, 'store']);

Route::get('user/{id}/scoreboard', [SoloGamesController::class, 'getScoreBoard'])->name('scoreboard');

Route::get('sologame/question/{n}', [SoloGamesQuestionsController::class, 'index'])->where('n', '[1,10]');

Route::get('multigame/question/{n}', [MultiGamesQuestionsController::class, 'index'])->where('n', '[1,15]');

