Api en laravel 8

installer le projet : composer install <br>
crétaion du .env sur modèle du .env.example
lancer le projet : php artisan serve <br>

Pour voir le fonctionnement des tables du projet, voir mcd dans dossier document à la racine du dossier

Description : une application web en localhost permettant de faire un quizz météo en mode solo ou multi joueur(s)

L'api permet, pour un jeu en mode solo,  de se connecter ou de s'inscrire grâce 
au module breeze. Une fois connecter l'utilisateur peut créer une partie ou consulter son scoreboard qui 
récupère uniquement ces parties en mode solo par ordre chronologique. Pour créer une partie, le joueur doir choisir une 
ville. 

Une fois la partie créée, l'api récupère les 10 premières questions qui on été le moins utilisé
afin de les renvoyer une par une. La partie réponse n'est pas gérer actuellement, cependant l'idée
était de d'avoir 3 réponses fausse récupérer depuis la table réponse et que la bonne réponse soit 
généré à partir des valeurs des tables qui gère la partie weather.

Le mode multi fonctionne de la même facon, sauf que chaque joueur choisit une ville et que le nombre
de questions est à choisir dans une liste.

